package vitalii.weatherforecast.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.TextureView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.libraries.places.api.Places
import dagger.hilt.android.AndroidEntryPoint
import vitalii.weatherforecast.R
import vitalii.weatherforecast.databinding.ActivityMainBinding

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val bottomNavigation by lazy {
        binding.bottomNavigationView
    }
    private val navHostFragment by lazy {
        findNavController(R.id.nav_host_fragment)
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUI()
    }

    private fun initUI() {
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_today_fragment,
                R.id.navigation_week_fragment,
                R.id.navigation_maps_fragment
            )
        )
        setupActionBarWithNavController(navController = navHostFragment, appBarConfiguration)
        bottomNavigation.setupWithNavController(navController = navHostFragment)
    }
}