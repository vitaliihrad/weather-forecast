package vitalii.weatherforecast.ui.map.geocoder

enum class ResponseStatus {
    SUCCESS,
    ERROR,
    LOADING
}