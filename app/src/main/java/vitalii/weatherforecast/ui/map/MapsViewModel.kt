package vitalii.weatherforecast.ui.map

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import vitalii.weatherforecast.ui.map.geocoder.GeoCoderUtil
import vitalii.weatherforecast.ui.map.geocoder.LoadDataCallback
import vitalii.weatherforecast.ui.map.geocoder.LocationModel
import vitalii.weatherforecast.ui.map.geocoder.ResponseStatusCallbacks
import javax.inject.Inject

typealias ResponseType = ResponseStatusCallbacks<LocationModel>

@HiltViewModel
class MapsViewModel @Inject constructor() : ViewModel() {

    private val _locationInformationLiveData = MutableLiveData<ResponseType>()
    val locationInformationLiveData: LiveData<ResponseType> = _locationInformationLiveData

    fun getLocationInfo(context: Context, latitude: String, longitude: String) {
        _locationInformationLiveData.postValue(ResponseStatusCallbacks.loading(data = null))
        GeoCoderUtil.execute(
            context = context,
            latitude = latitude,
            longitude = longitude,
            callback = object : LoadDataCallback<LocationModel> {

                override fun onDataLoaded(response: LocationModel) {
                    _locationInformationLiveData.postValue(ResponseStatusCallbacks.success(response))
                }

                override fun onDataNotAvailable(errorCode: Int, reasonMsg: String) {
                    _locationInformationLiveData.postValue(
                        ResponseStatusCallbacks.error(
                            data = null,
                            message = "Something went wrong!"
                        )
                    )
                }
            })
    }
}