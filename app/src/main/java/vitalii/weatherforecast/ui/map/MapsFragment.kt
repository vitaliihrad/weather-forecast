package vitalii.weatherforecast.ui.map

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import vitalii.weatherforecast.R
import vitalii.weatherforecast.data.repo.LocationHelperRepositoryImpl
import vitalii.weatherforecast.databinding.FragmentMapsBinding
import vitalii.weatherforecast.domain.repo.LocationHelperRepository
import vitalii.weatherforecast.extensions.showToast
import vitalii.weatherforecast.ui.base.BaseFragment
import vitalii.weatherforecast.ui.map.geocoder.ResponseStatus
import vitalii.weatherforecast.util.collection.getOrDefaultValue

@AndroidEntryPoint
class MapsFragment : BaseFragment<FragmentMapsBinding>(), OnMapReadyCallback,
    GoogleMap.OnCameraIdleListener {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentMapsBinding =
        FragmentMapsBinding::inflate

    private val viewModel by viewModels<MapsViewModel>()

    private val locationHelperRepository: LocationHelperRepository by lazy {
        LocationHelperRepositoryImpl(requireContext())
    }

    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        when {
            permissions.getOrDefaultValue(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                locationHelperRepository.setRequestLocationUpdate()
            }
            permissions.getOrDefaultValue(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                locationHelperRepository.setRequestLocationUpdate()
            }
            else -> {
                isAccessDenied()
            }
        }
    }

    var mGoogleMap: GoogleMap? = null
    var latLng: LatLng = LatLng(25.1933895, 66.5949836)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun setObserving() {
        super.setObserving()
        locationHelperRepository.locationLiveData.observe(viewLifecycleOwner) { location ->
            showToast(location.toString())
            // TODO: 16.06.2022 set marker LatLng
            locationHelperRepository.saveLocation(LatLng(location.latitude, location.longitude))
        }
        viewModel.locationInformationLiveData.observe(viewLifecycleOwner) {
            it?.let {
                when (it.status) {
                    ResponseStatus.ERROR -> {
                        binding.addressEditText.setText("Location not found!")
                    }
                    ResponseStatus.LOADING -> {
                        binding.addressEditText.setText("Searching...")
                    }
                    ResponseStatus.SUCCESS -> {
                        it.data?.let { model ->
                            binding.addressEditText.setText(model.locationAddress)
                        }
                    }
                }
            }
        }
    }

    override fun setListeners() {
        super.setListeners()
        binding.myLocationFab.setOnClickListener {
            showShouldRationale()
        }
    }

    private fun showShouldRationale() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
            || shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)
        ) {
            showLocationRequestDialog(
                title = getString(R.string.warning_dialog),
                message = getString(R.string.message_dialog)
            )
        } else {
            requestLocation()
        }
    }

    private fun showLocationRequestDialog(title: String, message: String) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(getString(R.string.text_alert_dialog_positive_btn)) { _, _: Int ->
                requestLocation()
            }
            .show()
    }

    private fun requestLocation() {
        locationPermissionRequest.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
    }

    private fun isAccessDenied() {
        AlertDialog.Builder(context)
            .setTitle(getString(R.string.warning_dialog))
            .setMessage(getString(R.string.access_denied_message_alert_dialog))
            .setPositiveButton(getString(R.string.give_access_alert_dialog_positive_btn)) { _, _ ->
                startActivity(Intent(Settings.ACTION_APPLICATION_SETTINGS))
            }
            .setNegativeButton(getString(R.string.denied_access_alert_dialog_negative_btn)) { _, _ ->
                // TODO: 15.06.2022 denied access
            }
            .show()
    }

    override fun onMapReady(map: GoogleMap) {
        map.let {
            mGoogleMap = it
            mGoogleMap?.clear()
            mGoogleMap?.setOnCameraIdleListener(this)
            animateCamera()
        }
    }

    private fun animateCamera() {
        mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    override fun onCameraIdle() {
        mGoogleMap?.let {
            it.cameraPosition.let { position ->
                latLng = mGoogleMap?.cameraPosition!!.target
                viewModel.getLocationInfo(
                    requireContext(),
                    latLng.latitude.toString(),
                    latLng.longitude.toString()
                )
            }
        }
    }
}