package vitalii.weatherforecast.ui.today

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.location.Geocoder
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import dagger.hilt.android.AndroidEntryPoint
import vitalii.weatherforecast.R
import vitalii.weatherforecast.data.repo.LocationHelperRepositoryImpl
import vitalii.weatherforecast.databinding.FragmentTodayBinding
import vitalii.weatherforecast.domain.model.Filter
import vitalii.weatherforecast.domain.model.Hourly
import vitalii.weatherforecast.domain.model.WeatherBase
import vitalii.weatherforecast.domain.repo.LocationHelperRepository
import vitalii.weatherforecast.extensions.showToast
import vitalii.weatherforecast.ui.base.BaseFragment
import vitalii.weatherforecast.util.getDateTimeFromUnix
import vitalii.weatherforecast.util.resultof.ResultOf
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class TodayFragment : BaseFragment<FragmentTodayBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentTodayBinding =
        FragmentTodayBinding::inflate

    private val viewModel by viewModels<TodayViewModel>()
    private val adapter = TodayAdapter()
    private val locationHelperRepository: LocationHelperRepository by lazy {
        LocationHelperRepositoryImpl(requireContext())
    }

    override fun initUi() {
        super.initUi()
        setHasOptionsMenu(true)
        binding.todayWeatherRecyclerView.adapter = adapter
        getGeocoding(locationHelperRepository.loadLocation())
    }

    override fun setObserving() {
        super.setObserving()
        viewModel
            .weatherLiveData.observe(viewLifecycleOwner) { resultOf ->
                when (resultOf) {
                    is ResultOf.Success -> updateValueOnScreen(resultOf.value)
                    is ResultOf.Failure -> resultOf.message?.let { showToast(it) }
                }
            }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_menu, menu)
        menu.findItem(R.id.toolbar_search_item).setOnMenuItemClickListener {
            searchLocate()
            true
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        Log.i(TAG, "Place: ${place}, ${place.id}")
                        updateLocationStorage(place)
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i(TAG, status.statusMessage ?: "")
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun searchLocate() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), getString(R.string.google_places_key))
        }
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
            .build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    private fun getGeocoding(latLang: LatLng) {
        getLastWeather(latLang)

        val geocoder = Geocoder(requireContext(), Locale.getDefault())
        val addressList = geocoder.getFromLocation(
            latLang.latitude,
            latLang.longitude,
            1
        )
        binding.cityTextView.text = addressList[0].locality
    }

    private fun updateLocationStorage(place: Place) {
        place.latLng?.let {
            getLastWeather(it)
        }
        binding.cityTextView.text = place.name
    }

    private fun getLastWeather(latLang: LatLng) {
        viewModel.getWeather(
            Filter(
                apiKey = getString(R.string.open_weather_api_key),
                latLong = latLang
            )
        )
    }

    @SuppressLint("SetTextI18n")
    private fun updateValueOnScreen(weatherBase: WeatherBase) {
        adapter.submit(weatherBase.hourly)
        binding.apply {
            cloudsTextView.text = "${weatherBase.current.clouds}%"
            descriptionTextView.text = weatherBase.current.weather[0].description
            humidityTextView.text = "${weatherBase.current.humidity}%"
            lastUpdateDataTextView.text =
                getDateTimeFromUnix(weatherBase.current.dt, SimpleDateFormat.getDateTimeInstance())
            temperatureTextView.text =
                getString(R.string.temperature_celsius, weatherBase.current.temp.toString())
            windSpeedTextView.text =
                getString(R.string.wind_speed, weatherBase.current.wind_speed.toString())
            weatherGraphView.addSeries(createSeries(weatherBase.hourly))
        }
    }

    private fun createSeries(data: List<Hourly>): LineGraphSeries<DataPoint> {
        val series = LineGraphSeries<DataPoint>()
        var step = 0.0
        data.forEach { hourly ->
            series.appendData(
                DataPoint(step++, hourly.temp), true, data.size
            )
        }
        return series
    }

    companion object {

        private const val AUTOCOMPLETE_REQUEST_CODE = 1
    }
}