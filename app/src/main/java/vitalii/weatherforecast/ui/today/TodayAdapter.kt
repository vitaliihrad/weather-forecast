package vitalii.weatherforecast.ui.today

import android.view.View
import vitalii.weatherforecast.R
import vitalii.weatherforecast.domain.model.Hourly
import vitalii.weatherforecast.ui.base.BaseAdapter

class TodayAdapter : BaseAdapter<Hourly>() {

    override fun getViewHolder(view: View) = TodayViewHolder(view)

    override fun getItemRes() = R.layout.item_hourly_weather
}