package vitalii.weatherforecast.ui.today

import android.view.View
import vitalii.weatherforecast.R
import vitalii.weatherforecast.databinding.ItemHourlyWeatherBinding
import vitalii.weatherforecast.domain.model.Hourly
import vitalii.weatherforecast.ui.base.BaseViewHolder
import vitalii.weatherforecast.util.getDateTimeFromUnix
import java.text.SimpleDateFormat

class TodayViewHolder(view: View) : BaseViewHolder<Hourly>(view) {

    private val binding = ItemHourlyWeatherBinding.bind(itemView)
    private val res = view.context.resources

    override fun bind(data: Hourly) {
        with(binding) {
            iconWeatherImageView.setImageResource(0)
            iconWeatherImageView.setImageResource(R.drawable.ic_sunny)
            temperatureTextViewItemHourlyWeather.text =
                res.getString(R.string.temperature_celsius, data.temp.toString())
            timeTextView.text =
                getDateTimeFromUnix(dateUnix = data.dt, dataFormat = SimpleDateFormat.getTimeInstance())
        }
    }
}