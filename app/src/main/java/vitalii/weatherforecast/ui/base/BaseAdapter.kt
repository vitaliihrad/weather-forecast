package vitalii.weatherforecast.ui.base

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T>(
    private val onClick: ((position: Int) -> Unit)? = null
) : RecyclerView.Adapter<BaseViewHolder<T>>() {

    private val data = mutableListOf<T>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(getItemRes(), parent, false)
        return getViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount() = data.size

    abstract fun getViewHolder(view: View): BaseViewHolder<T>

    abstract fun getItemRes(): Int

    fun getItem(position: Int) = data[position]

    @SuppressLint("NotifyDataSetChanged")
    fun submit(data: Collection<T>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }
}