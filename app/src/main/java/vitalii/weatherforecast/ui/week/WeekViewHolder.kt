package vitalii.weatherforecast.ui.week

import android.view.View
import vitalii.weatherforecast.R
import vitalii.weatherforecast.databinding.ItemWeatherForWeekBinding
import vitalii.weatherforecast.domain.model.Daily
import vitalii.weatherforecast.ui.base.BaseViewHolder
import vitalii.weatherforecast.util.getDateTimeFromUnix
import java.text.SimpleDateFormat

class WeekViewHolder(view: View) : BaseViewHolder<Daily>(view) {

    private val binding = ItemWeatherForWeekBinding.bind(itemView)
    private val res = view.context.resources

    override fun bind(data: Daily) {
        with(binding) {
            dateTextView.text = getDateTimeFromUnix(data.dt, SimpleDateFormat.getDateInstance())
            iconWeatherForWeekImageView.setImageResource(R.drawable.ic_sunny)
            temperatureMinMaxTextView.text = res.getString(
                R.string.min_max_temperature,
                data.temp.min.toString(),
                data.temp.max.toString()
            )
        }
    }
}