package vitalii.weatherforecast.ui.week

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vitalii.weatherforecast.domain.model.Filter
import vitalii.weatherforecast.domain.model.WeatherBase
import vitalii.weatherforecast.domain.repo.WeatherRepository
import vitalii.weatherforecast.util.resultof.ResultOf
import javax.inject.Inject

@HiltViewModel
class WeekViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository
) : ViewModel() {

    private val _weatherLiveData = MutableLiveData<ResultOf<WeatherBase>>()
    val weatherLiveData: LiveData<ResultOf<WeatherBase>> = _weatherLiveData

    fun getWeather(filter: Filter) {
        viewModelScope.launch(Dispatchers.IO) {
            _weatherLiveData.postValue(weatherRepository.getWeather(filter))
        }
    }
}