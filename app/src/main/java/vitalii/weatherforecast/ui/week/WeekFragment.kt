package vitalii.weatherforecast.ui.week

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import vitalii.weatherforecast.R
import vitalii.weatherforecast.databinding.FragmentWeekBinding
import vitalii.weatherforecast.domain.model.Filter
import vitalii.weatherforecast.extensions.showToast
import vitalii.weatherforecast.ui.base.BaseFragment
import vitalii.weatherforecast.util.resultof.ResultOf

@AndroidEntryPoint
class WeekFragment : BaseFragment<FragmentWeekBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentWeekBinding =
        FragmentWeekBinding::inflate
    private val viewModel by viewModels<WeekViewModel>()
    private val adapter = WeekAdapter()

    override fun initUi() {
        super.initUi()
        binding.weekRecyclerView.adapter = adapter
        viewModel.getWeather(
            Filter(
                apiKey = getString(R.string.open_weather_api_key),
                latLong = LatLng(48.66227963755275, 33.08042373282633)
            )
        )
    }

    override fun setObserving() {
        super.setObserving()
        viewModel.weatherLiveData.observe(viewLifecycleOwner) { resultOf ->
            when (resultOf) {
                is ResultOf.Success -> adapter.submit(resultOf.value.daily)
                is ResultOf.Failure -> resultOf.message?.let { showToast(it) }
            }
        }
    }
}