package vitalii.weatherforecast.ui.week

import android.view.View
import vitalii.weatherforecast.R
import vitalii.weatherforecast.domain.model.Daily
import vitalii.weatherforecast.ui.base.BaseAdapter

class WeekAdapter : BaseAdapter<Daily>() {

    override fun getViewHolder(view: View) = WeekViewHolder(view)

    override fun getItemRes() = R.layout.item_weather_for_week
}