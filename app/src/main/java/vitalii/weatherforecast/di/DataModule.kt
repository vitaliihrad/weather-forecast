package vitalii.weatherforecast.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import vitalii.weatherforecast.data.network.api.OpenWeatherApiService
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DataModule {

    @Provides
    @Singleton
    fun provideOpenWeatherApi(@ApplicationContext context: Context) =
        OpenWeatherApiService.create(context)
}