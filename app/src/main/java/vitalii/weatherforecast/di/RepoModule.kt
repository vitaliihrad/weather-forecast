package vitalii.weatherforecast.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import vitalii.weatherforecast.data.repo.WeatherRepositoryImpl
import vitalii.weatherforecast.domain.repo.WeatherRepository

@InstallIn(ViewModelComponent::class)
@Module
abstract class RepoModule {

    @Binds
    @ViewModelScoped
    abstract fun provideWeatherRepo(repo: WeatherRepositoryImpl): WeatherRepository
}