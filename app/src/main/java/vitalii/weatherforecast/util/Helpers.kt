package vitalii.weatherforecast.util

import java.text.DateFormat
import java.util.*

fun getDateTimeFromUnix(dateUnix: Int, dataFormat: DateFormat): String? =
    dataFormat.format(Date(dateUnix.toLong() * 1000))