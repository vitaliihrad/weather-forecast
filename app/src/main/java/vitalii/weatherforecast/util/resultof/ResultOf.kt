package vitalii.weatherforecast.util.resultof

sealed class ResultOf<out T> {

    data class Success<out R>(val value: R) : ResultOf<R>()

    data class Failure(
        val throwable: Throwable,
        val message: String? = null
    ) : ResultOf<Nothing>()
}