package vitalii.weatherforecast.util.resultof

import vitalii.weatherforecast.util.network.NoConnectionInterceptor

suspend fun <T> safeCall(
    networkRequest: suspend () -> T
): ResultOf<T> {
    return try {
        ResultOf.Success(networkRequest())
    } catch (e: NoConnectionInterceptor.NoInternetException) {
        ResultOf.Failure(e, "Проверьте подключение")
    } catch (e: NoConnectionInterceptor.NoConnectivityException) {
        ResultOf.Failure(e, "Интернет недоступен")
    }
}
