package vitalii.weatherforecast.util.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

class NoConnectionInterceptor(private val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return if (!isConnectionOn()) {
            throw NoConnectivityException()
        } else if (!isInternetAvailable()) {
            throw NoInternetException()
        } else {
            chain.proceed(chain.request())
        }
    }

    private fun isConnectionOn(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork
        val connection = connectivityManager.getNetworkCapabilities(network)
        return connection != null && (
                connection.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                        connection.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
    }

    private fun isInternetAvailable(): Boolean {
        return try {
            val sock = Socket()
            val sockAddress = InetSocketAddress(HOST_NAME, PORT)
            sock.use { socket ->
                socket.connect(sockAddress, TIME_OUT_MS)
            }
            true
        } catch (e: IOException) {
            false
        }
    }

    class NoConnectivityException : IOException() {

        override val message: String
            get() = "No network available, please check your WiFi or Data connection"
    }

    class NoInternetException : IOException() {

        override val message: String
            get() = "No internet available, please check your connected WIFi or Data"
    }

    companion object {

        private const val TIME_OUT_MS: Int = 1500
        private const val HOST_NAME: String = "8.8.8.8"
        private const val PORT: Int = 53
    }
}