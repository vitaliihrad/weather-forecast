package vitalii.weatherforecast.domain.repo

import vitalii.weatherforecast.domain.model.Filter
import vitalii.weatherforecast.domain.model.WeatherBase
import vitalii.weatherforecast.util.resultof.ResultOf

interface WeatherRepository {

    suspend fun getWeather(filters: Filter): ResultOf<WeatherBase>
}