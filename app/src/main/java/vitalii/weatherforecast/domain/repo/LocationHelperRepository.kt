package vitalii.weatherforecast.domain.repo

import android.location.Location
import androidx.lifecycle.LiveData
import com.google.android.gms.maps.model.LatLng

interface LocationHelperRepository {

    val locationLiveData: LiveData<Location>

    fun removeUpdateLocationManager()

    fun setRequestLocationUpdate()

    fun saveLocation(latLng: LatLng)

    fun loadLocation(): LatLng
}