package vitalii.weatherforecast.domain.model

import com.google.android.gms.maps.model.LatLng

class Filter(
    val apiKey: String,
    val latLong: LatLng,
    val lang: String? = "ru",
    val units: String? = "metric",
    val exclude: String? = "minutely",
)