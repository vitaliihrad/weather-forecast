package vitalii.weatherforecast.domain.model.extensions

import vitalii.weatherforecast.data.network.model.*
import vitalii.weatherforecast.domain.model.*

fun WeatherBaseResponse.toWeatherBase(): WeatherBase {
    return WeatherBase(
        lat = lat,
        lon = lon,
        timezone = timezone,
        timezone_offset = timezone_offset,
        current = current.toCurrent(),
        hourly = hourly.map { hourlyResponse ->
            hourlyResponse.toHourly()
        },
        daily = daily.map { dailyResponse ->
            dailyResponse.toDaily()
        },
    )
}

fun CurrentResponse.toCurrent() = Current(
    dt = dt,
    sunrise = sunrise,
    sunset = sunset,
    temp = temp,
    feels_like = feels_like,
    pressure = pressure,
    humidity = humidity,
    dew_point = dew_point,
    uvi = uvi,
    clouds = clouds,
    visibility = visibility,
    wind_speed = wind_speed,
    wind_deg = wind_deg,
    wind_gust = wind_gust,
    weather = weather.map { weatherResponse ->
        weatherResponse.toWeather()
    }
)

fun HourlyResponse.toHourly() = Hourly(
    dt = dt,
    temp = temp,
    feels_like = feels_like,
    pressure = pressure,
    humidity = humidity,
    dew_point = dew_point,
    uvi = uvi,
    clouds = clouds,
    visibility = visibility,
    wind_speed = wind_speed,
    wind_deg = wind_deg,
    wind_gust = wind_gust,
    weather = weather.map { weatherResponse ->
        weatherResponse.toWeather()
    },
    pop = pop
)

fun DailyResponse.toDaily() = Daily(
    dt = dt,
    sunrise = sunrise,
    sunset = sunset,
    moonrise = moonrise,
    moonset = moonset,
    moon_phase = moon_phase,
    temp = temp,
    feels_like = feels_like.toFeelsLike(),
    pressure = pressure,
    humidity = humidity,
    dew_point = dew_point,
    wind_speed = wind_speed,
    wind_deg = wind_deg,
    wind_gust = wind_gust,
    weather = weather.map { weatherResponse ->
        weatherResponse.toWeather()
    },
    clouds = clouds,
    pop = pop,
    uvi = uvi
)

fun FeelsLikeResponse.toFeelsLike() = FeelsLike(
    day = day,
    night = night,
    eve = eve,
    morn = morn
)

fun WeatherResponse.toWeather() = Weather(
    id = id,
    main = main,
    description = description,
    icon = icon
)


