package vitalii.weatherforecast.data.repo

import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import vitalii.weatherforecast.domain.repo.LocationHelperRepository

class LocationHelperRepositoryImpl(
    private val context: Context
) : LocationHelperRepository {

    private val _locationLiveData = MutableLiveData<Location>()
    override val locationLiveData = _locationLiveData
    private val locationManager by lazy {
        context.getSystemService(Context.LOCATION_SERVICE) as? LocationManager
    }
    private val gpsLocationListener = LocationListener { location ->
        removeUpdateLocationManager()
        _locationLiveData.postValue(location)
    }
    private val networkLocationListener = LocationListener { location ->
        removeUpdateLocationManager()
        _locationLiveData.postValue(location)
    }

    override fun removeUpdateLocationManager() {
        locationManager?.removeUpdates(gpsLocationListener)
        locationManager?.removeUpdates(networkLocationListener)
    }

    override fun setRequestLocationUpdate() {
        val hasGps = locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val hasNetwork = locationManager?.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        try {
            if (hasGps == true) {
                Log.i("Location", "Start request location update (GPS)")
                locationManager?.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    MIN_TIME_MS,
                    MIN_DISTANCE_M,
                    gpsLocationListener
                )
            }
            if (hasNetwork == true) {
                Log.i("Location", "Start request location update (network)")
                locationManager?.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    MIN_TIME_MS,
                    MIN_DISTANCE_M,
                    networkLocationListener
                )
            }
        } catch (e: SecurityException) {
            Log.e("Location:", e.toString())
        }
    }

    override fun saveLocation(latLng: LatLng) {
        context.getSharedPreferences(KEY_LOCATION, Context.MODE_PRIVATE)
            .edit()
            .putFloat(KEY_LOCATION_LATITUDE, latLng.latitude.toFloat())
            .putFloat(KEY_LOCATION_LONGITUDE, latLng.longitude.toFloat())
            .apply()
    }

    override fun loadLocation(): LatLng {
        with(context.getSharedPreferences(KEY_LOCATION, Context.MODE_PRIVATE)) {
            return LatLng(
                getFloat(KEY_LOCATION_LATITUDE, 0.0f).toDouble(),
                getFloat(KEY_LOCATION_LONGITUDE, 0.0f).toDouble()
            )
        }
    }

    companion object {

        private const val MIN_TIME_MS: Long = 5000
        private const val MIN_DISTANCE_M: Float = 0F
        private const val KEY_LOCATION: String = "locationSharedPref"
        private const val KEY_LOCATION_LATITUDE: String = "locationLatitudeSharedPref"
        private const val KEY_LOCATION_LONGITUDE: String = "locationLongitudeSharedPref"
    }
}