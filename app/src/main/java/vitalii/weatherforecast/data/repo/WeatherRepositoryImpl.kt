package vitalii.weatherforecast.data.repo

import vitalii.weatherforecast.data.network.api.OpenWeatherApiService
import vitalii.weatherforecast.data.network.model.getFiltersMapOf
import vitalii.weatherforecast.domain.model.Filter
import vitalii.weatherforecast.domain.model.WeatherBase
import vitalii.weatherforecast.domain.model.extensions.toWeatherBase
import vitalii.weatherforecast.domain.repo.WeatherRepository
import vitalii.weatherforecast.util.resultof.ResultOf
import vitalii.weatherforecast.util.resultof.safeCall
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(
    private val openWeatherApiService: OpenWeatherApiService,
) : WeatherRepository {

    override suspend fun getWeather(filters: Filter): ResultOf<WeatherBase> {
        return safeCall {
            openWeatherApiService.getWeather(filters.getFiltersMapOf()).toWeatherBase()
        }
    }
}