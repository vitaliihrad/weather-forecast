package vitalii.weatherforecast.data.network.model

import com.google.gson.annotations.SerializedName

data class WeatherBaseResponse(
    @SerializedName("lat") val lat: Double,
    @SerializedName("lon") val lon: Double,
    @SerializedName("timezone") val timezone: String,
    @SerializedName("timezone_offset") val timezone_offset: Int,
    @SerializedName("current") val current: CurrentResponse,
    @SerializedName("hourly") val hourly: List<HourlyResponse>,
    @SerializedName("daily") val daily: List<DailyResponse>
)