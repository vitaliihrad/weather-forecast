package vitalii.weatherforecast.data.network.model

import vitalii.weatherforecast.data.network.api.OpenWeatherApiService
import vitalii.weatherforecast.domain.model.Filter

fun Filter.getFiltersMapOf(): Map<String, Any> {
    val filtersMap = mapOf<String, Any?>(
        OpenWeatherApiService.QUERY_API_KEY to apiKey,
        OpenWeatherApiService.QUERY_EXCLUDE to exclude,
        OpenWeatherApiService.QUERY_LANGUAGE to lang,
        OpenWeatherApiService.QUERY_UNITS to units,
        OpenWeatherApiService.QUERY_LATITUDE to latLong.latitude,
        OpenWeatherApiService.QUERY_LONGITUDE to latLong.longitude
    )
    return filtersMap
        .mapNotNull { p -> p.value?.let { Pair(p.key, it) } }
        .toMap()
}
