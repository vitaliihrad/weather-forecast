package vitalii.weatherforecast.data.network.api

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap
import vitalii.weatherforecast.data.network.model.WeatherBaseResponse
import vitalii.weatherforecast.util.network.NoConnectionInterceptor

interface OpenWeatherApiService {

    @GET("2.5/onecall?")
    @JvmSuppressWildcards
    suspend fun getWeather(@QueryMap filters: Map<String, Any>): WeatherBaseResponse

    companion object {

        const val QUERY_LATITUDE: String = "lat"
        const val QUERY_LONGITUDE: String = "lon"
        const val QUERY_EXCLUDE: String = "exclude"
        const val QUERY_API_KEY: String = "appid"
        const val QUERY_LANGUAGE: String = "lang"
        const val QUERY_UNITS: String = "units"
        private const val BASE_URL = "https://api.openweathermap.org/data/"

        fun create(context: Context): OpenWeatherApiService {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
            val noConnectionInterceptor = NoConnectionInterceptor(context)
            val client: OkHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(noConnectionInterceptor)
                .build()
            return Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(OpenWeatherApiService::class.java)
        }
    }
}